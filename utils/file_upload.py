
import logging
import requests
import os
import uuid

def upload_images(imgs_list: list, upload_api: str, token: str) -> dict:
    """upload_images using the  given upload api

    Args:
        imgs_list (list): list of images to upload
        upload_api (str): the api for uploading the images to s3
        token (str): token for uploading

    Returns:
        keys (dict): dict of submitted imagenames,
                received keys, and created uuid names.
    """
    keys = {"keys": [], "img": [], "uuid": []}

    if not isinstance(imgs_list, list):
        raise ValueError(
            "Expecting list for image_list but received %s", type(img_list)
        )
    headers = {"Authorization": token}
    for img in imgs_list:

        # try:
            directory = os.getcwd()
            img = os.path.join(directory,"utils",img)
            print(img)
            files = {"file": open(img, "rb")}
            # creating random strings for name as s3 compatible names
            uuid_ = "-".join(str(uuid.uuid4()).split("-")[:3]) + ".jpg"
            data = {"filename": uuid_}
            print(files,data,headers)
            response_ = requests.post(
                upload_api, files=files, data=data, headers=headers
            )
            print("upload endpoint:%s", upload_api)
            print("Response from upload  api:%s", response_)
            print("Successfully uploaded the images...")
            keys["keys"].append(response_.json()["data"]["key"])
            keys["img"].append(img)
            keys["uuid"].append(uuid_)
        # except:
        #     logging.error("Failed to upload image %s using upload API!", img)
        #     continue
    print(keys)
    return keys



imgs_list = ["1.jpg","2.jpg"]
upload_api = 'https://api.dev.claimgenius.com/files/upload'
token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6OCwiY2xpZW50X2lkIjoxLCJzY29wZSI6Imphc21pbmUvYXBwcmFpc2VyL2xlYWQiLCJyZWdpb24iOiJVUyIsImlhdCI6MTY0MzM0OTE5N30.t6X-CoFTeYX4DuhZ85WhQNm3BIfFHtif3Ys-IRIlpxw'


keys = upload_images(imgs_list, upload_api, token )
print(keys)