import time
begin = time.time()
import cv2
import os 
import urllib.request
import numpy as np
from skimage import color
# from constants import CONFIG_DATA, MODEL_NAME, brightness_weightage, sharpness_weightage

sharpness_weightage= 0.5
brightness_weightage= 0.5

class Model():
    def read_image(image_url):
        req = urllib.request.urlopen(image_url)
        arr = np.asarray(bytearray(req.read()), dtype=np.uint8)
        img = cv2.imdecode(arr, -1) 
        return(img)
 
            

    def brightness(image_url):
        image = cv2.cvtColor(Model.read_image(image_url), cv2.COLOR_BGR2HSV)
        img_scale = (image + [0, 0, 0])/[180,255,100]
        img = cv2.split(img_scale)
        luminosity = np.average(img[0].flatten())

        if (0.1 <= luminosity < 0.2):
            return(0.2)
        elif ( 0.2 <= luminosity < 0.3):
            return(0.4)
                
        elif ( 0.3 <= luminosity < 0.4 or 0.6 <= luminosity < 0.7):
            return(0.8)
                
        elif ( 0.4 <= luminosity <= 0.6):
            return(1)

        elif ( 0.7 <= luminosity < 0.8):
            return(0.6)
        else:
            return(0.1)

 

    def smoothenImage(image, transpose = False, epsilon = 1e-8):
            #print('SmoothenImage')
            fil = np.array([0.5, 0, -0.5]) 
            if transpose:
                image = image.T
            image_smoothed = np.array([np.convolve(image[i], fil, mode="same") for i in range(image.shape[0])])
            if transpose:
                image_smoothed = image_smoothed.T
            image_smoothed = np.abs(image_smoothed)/(np.max(image_smoothed) + epsilon)
            return image_smoothed
        
    def sharpness_measure(image_url, width=2, sharpness_threshold=2, debug=False, epsilon = 1e-8,blur=False, blurSize=(5,5)): 
        s = Model.read_image(image_url)
        if isinstance(s, str):
            if os.path.exists(s):
                image = cv2.imread(s, cv2.IMREAD_GRAYSCALE)
            else:
                raise FileNotFoundError('Image is not found on your system')
        elif isinstance(s, np.ndarray):
            if len(s.shape) == 3:
                image = cv2.cvtColor(s, cv2.COLOR_BGR2GRAY)
            elif len(s.shape) == 2:
                image = s
            else:
                raise ValueError('Image is not in correct shape')
        else:
            raise ValueError('Only image can be passed to the constructor')

        if blur:
            image = cv2.GaussianBlur(image, blurSize)
        Im = cv2.medianBlur(image, 3, cv2.CV_64F).astype("double")/255.0
        edge_threshold=0.0001
        smoothx = Model.smoothenImage(image, transpose=True)
        smoothy = Model.smoothenImage(image)
        edgex = smoothx > edge_threshold
        edgey = smoothy > edge_threshold
        median_shift_up = np.pad(Im, ((0,2), (0,0)), 'constant')[2:,:]
        median_shift_down = np.pad(Im, ((2,0), (0,0)), 'constant')[:-2,:]
        domx = np.abs(median_shift_up - 2*Im + median_shift_down)
        median_shift_left = np.pad(Im, ((0,0), (0,2)), 'constant')[:,2:]
        median_shift_right = np.pad(Im, ((0,0), (2,0)), 'constant')[:,:-2]
        domy = np.abs(median_shift_left - 2*Im + median_shift_right)
        Cx = np.abs(Im - np.pad(Im, ((1,0), (0,0)), 'constant')[:-1, :])
        Cy = np.abs(Im - np.pad(Im, ((0,0), (1,0)), 'constant')[:, :-1])
        Cx = np.multiply(Cx, edgex)
        Cy = np.multiply(Cy, edgey)
        Sx = np.zeros(domx.shape)
        Sy = np.zeros(domy.shape)
        for i in range(width, domx.shape[0]-width):
            num = np.abs(domx[i-width:i+width, :]).sum(axis=0)
            dn = Cx[i-width:i+width, :].sum(axis=0)
            Sx[i] = [(num[k]/dn[k] if dn[k] > 1e-3 else 0) for k in range(Sx.shape[1])]
        for j in range(width, domy.shape[1]-width):
            num = np.abs(domy[:, j-width: j+width]).sum(axis=1)
            dn = Cy[:, j-width:j+width].sum(axis=1)
        Sy[:, j] = [(num[k]/dn[k] if dn[k] > 1e-3 else 0) for k in range(Sy.shape[0])]                      
        Sx = np.multiply(Sx, edgex)
        Sy = np.multiply(Sy, edgey)      
        n_sharpx = np.sum(Sx >= sharpness_threshold)
        n_sharpy = np.sum(Sy >= sharpness_threshold)
        n_edgex = np.sum(edgex)
        n_edgey = np.sum(edgey)     
        Rx = n_sharpx/(n_edgex + epsilon)
        Ry = n_sharpy/(n_edgey + epsilon)
        score = np.sqrt(Rx**2 + Ry**2)    
        if ( 0.90 <= score < 1.10):
            return(1)

        elif ( 0.85 <= score < 0.90 or  1.10 <= score <= 1.125):
            return(0.9)
                
        elif ( 0.80 <= score < 0.85 or 1.125 <= score< 1.15):
            return(0.8)

        elif ( 0.70 <= score < 0.80 or 1.15 <= score <= 1.175):
            return(0.7)
                
        elif ( 0.60 <= score < 0.70 or 1.175 <= score <= 1.2):
            return(0.6)

        elif ( 0.50 <= score < 0.60 or 1.2 <= score <= 1.22):
            return(0.5)
                
        elif ( 0.40 <= score < 0.50 or 1.22 <= score<= 1.24):
            return(0.4)

        elif ( 0.30 <= score < 0.40 or 1.24 <= score <= 1.28):
            return(0.3)

        elif ( 0.20 <= score < 0.30 or 1.28 <= score <= 1.30):
            return(0.2)

        else:
            return(0.1)



    def iqi(image_url, brightness_factor, sharpness_measure):
        x = float(brightness_weightage)*brightness_factor + float(sharpness_weightage)*sharpness_measure
        x=(x-0.35)/(0.9-0.35)
        if x<0:
            x=0
        if x>1:
            x=1
        x=x*100
        time.sleep(1)
        end = time.time()
        
        time_taken = {end - begin}
        print('time_taken', time_taken)

        return{'iqi': float(x) , 'brightness_factor': float(brightness_factor) , 'sharpness_factor': float(sharpness_measure)}



