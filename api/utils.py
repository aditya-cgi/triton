"""
This file is for creating utility to run DS Cluster for the given claims
"""

import os
import json
import pandas as pd
import requests
import time
# api secret for chola
#       x_api_secret = 'IuF99HZ8THbipA1sA9f8'
#       x_api_id  = 'mszglgPDo21ttER2EUEZ'
#  api secent for client 40
#       x_api_secret = 'lzHEPjARZW8CFy0BGXLv'
#       x_api_id  = 'scG5K2tSt3BUNKjxhGnx'

# ROBIN
# API ID :- RuZLps3yJX5j4GYdVBKF
# SECRET KEY :- Mg4KEAbnTemkR2JQ3atCCLIENT 
# client_id: 54

# UNIT
# API ID :- MwiH6IViGAlKdVjklMx9
# SECRET KEY :-ltIVKwyeKBUKS5LWnb9h
# client_id: 72

# teamrobin = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MzM1LCJjbGllbnRfaWQiOjcyLCJzY29wZSI6IiIsInJlZ2lvbiI6IlVTIiwiaWF0IjoxNjQyNDE4NjMyfQ.hfynXet6CenAHlKfBjML_mP0-qF1bzPkaVGO-lKWD5g'

# test = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6OCwiY2xpZW50X2lkIjoxLCJzY29wZSI6Imphc21pbmUvYXBwcmFpc2VyL2xlYWQiLCJyZWdpb24iOiJVUyIsImlhdCI6MTY0MzM0OTE5N30.t6X-CoFTeYX4DuhZ85WhQNm3BIfFHtif3Ys-IRIlpxw'

def process_one_claim(config, claim_data):
    method = 'POST'
    x_api_secret = 'cgqV9TmpUTZ2qru1Umvg'
    x_api_id = 'RuZLps3yJX5j4GYdVBKF'
    api = config['base_pipeline']
    pipeline = config['submit_claim_to']
    api_endpoint = api+pipeline
    print(x_api_id,x_api_secret)
    print("api_endpoint", api_endpoint)
    header = {'Content-Type': 'application/json',
              'Authorization':'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6OCwiY2xpZW50X2lkIjoxLCJzY29wZSI6Imphc21pbmUvYXBwcmFpc2VyL2xlYWQiLCJyZWdpb24iOiJVUyIsImlhdCI6MTY0MzM0OTE5N30.t6X-CoFTeYX4DuhZ85WhQNm3BIfFHtif3Ys-IRIlpxw'
 
#                 'x-api-id' : x_api_id,
#                 'x-api-secret' : x_api_secret,            
#                 'x-auth-client-id': '72'
                }

    payload = claim_data
    

    response = requests.request(method,api_endpoint, json= payload, headers=header).json()
    print("response",response)
    return response

def get_sedna_input(config, uid):
    method = 'GET'
    x_api_secret = 'cgqV9TmpUTZ2qru1Umvg'
    x_api_id = 'ToHX6rvtewTgAosr19wG'
    api = config['base_pipeline']
    pipeline = "requests/"+uid+"/sedna-payload"
    api_endpoint = api+pipeline
    # print(x_api_id,x_api_secret)
    header = {'Content-Type': 'application/json',
                            'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6OCwiY2xpZW50X2lkIjoxLCJzY29wZSI6Imphc21pbmUvYXBwcmFpc2VyL2xlYWQiLCJyZWdpb24iOiJVUyIsImlhdCI6MTY0MzM0OTE5N30.t6X-CoFTeYX4DuhZ85WhQNm3BIfFHtif3Ys-IRIlpxw'
#                 'x-api-id' : x_api_id,
#                 'x-api-secret' : x_api_secret,
#                 'x-auth-client-id': 'client_72'
                }


    response = requests.request(method,api_endpoint, headers=header).json()
    return response


def get_claim(config, uid):
    method = 'GET'
    x_api_secret = 'cgqV9TmpUTZ2qru1Umvg'
    x_api_id = 'ToHX6rvtewTgAosr19wG'
    api = config['base_pipeline']
    pipeline = "requests/"+uid+"?polygons=y"
    api_endpoint = api+pipeline
    # print(x_api_id,x_api_secret)
    header = {'Content-Type': 'application/json',
             'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6OCwiY2xpZW50X2lkIjoxLCJzY29wZSI6Imphc21pbmUvYXBwcmFpc2VyL2xlYWQiLCJyZWdpb24iOiJVUyIsImlhdCI6MTY0MzM0OTE5N30.t6X-CoFTeYX4DuhZ85WhQNm3BIfFHtif3Ys-IRIlpxw'
#                 'x-api-id' : x_api_id,
#                 'x-api-secret' : x_api_secret,
#                 'x-auth-client-id': 'client_72'
                }


    response = requests.request(method,api_endpoint, headers=header).json()
    return response


def prepare_claim_data(config):
    print(config['file_to_process'])
    df = pd.read_csv(config['file_to_process'])
    df["time"] = pd.to_datetime(df["time"])
    df = df.sort_values(by="time",ascending=False)
    for claim_number in df[:1].claim_id:
        claim_data = dict()
        images = df[df['claim_id'] == claim_number].image_url.tolist()
        images_list = []
        count = 0
        for image in images:
            if any(image.endswith(x) for x in ('.jpg','.JPG','.jpeg','JPEG') ):
                count +=1
                temp = {'uid' : str(count), 'url' : image}
                images_list.append(temp)

        claim_data = {'claim_number' : claim_number,
        'make' : 'NISSAN',
        'model' : 'VERSA',
        'mileage' : "12054",
        'year' : "2020",
        'state' : "CA",
        'country' : "US",
        'vin' : "JTDBT123530280421",
        'loss_year' : "2021",
        'force' : True,
        'report_date' : "2022-01-01",
        'images' : images_list}
        json_object = json.dumps(claim_data, indent = 4) 
        # Writing to sample.json 
        print(json_object)
        with open(os.path.join(config['save_json_to'],claim_number+".json"), "w") as outfile: 
            outfile.write(json_object) 
    return os.path.join(config['save_json_to'],claim_number+".json")
        
