from email import utils
from genericpath import exists
import json
import pandas as pd
import os
from pathlib import Path
import base64
import requests
import datetime
from io import BytesIO
import uuid
import iqi_generator
import csv
import run_pipeline


## Load config file
with open("config.json") as f:
    CONFIG_DATA = json.load(f)
feature_list = CONFIG_DATA["feature_list"]
allowed_part_list = CONFIG_DATA["allowed_parts_list"]

### CLAIM LEVEL OUTPUT ###

def calculate_damage_score(df):
    """
    This method calculates damage score
    """
    df["dent_ratio"] = df["dent_area"] / df["part_area"]
    df["scratch_ratio"] = df["scratch_area"] / df["part_area"]
    df["tear_ratio"] = df["tear_area"] / df["part_area"]
    df.loc[df['scratch_ratio'] > 1, 'scratch_ratio'] = 1
    df.loc[df['dent_ratio'] > 1, 'dent_ratio'] = 1
    df.loc[df['tear_ratio'] > 1, 'tear_ratio'] = 1
    df["rr_cnn_confidence"] = 1 - df["rr_cnn_confidence"]  # done to ensure p(repair)<0.5 and p(replace)>0.5
    df["dnd_cnn_confidence"] = 1 - df["dnd_cnn_confidence"]  # done to ensure p(no_damage)<0.5 and p(damage)>0.5
    damage_score_li = []
    for i in range(len(df)):
        acc_dnd = CONFIG_DATA[df.part_name.iloc[i]]["acc_dnd"]
        acc_tear = CONFIG_DATA[df.part_name.iloc[i]]["acc_tear"]
        acc_dent = CONFIG_DATA[df.part_name.iloc[i]]["acc_dent"]
        acc_scratch = CONFIG_DATA[df.part_name.iloc[i]]["acc_scratch"]
        acc_rrcnn = CONFIG_DATA[df.part_name.iloc[i]]["acc_rrcnn"]

        """
        Score confidence is calculated as weighted sum of dnd_cnn_confidence and rr_cnn_confidence.
        Accuracy of dnd model and rr_cnn model is approximately considered as weights.
        """
        score_confidence = df["dnd_cnn_confidence"] * acc_dnd + df["rr_cnn_confidence"] * acc_rrcnn

        """
        Ensuring scaled SDT counts are between 0 and 1 by diving with SDT divisor.
        Taking min to ensure they do not exceed 1. 
        SDT divisor is calculated by taking max of it's count which is occurring in atleast 1% of the data.
        """
        scaled_total_scratch_count = min(
            df.scratch_count.iloc[i] / CONFIG_DATA[df.part_name.iloc[i]]["scratch_divisor"], 1)
        scaled_total_dent_count = min(df.dent_count.iloc[i] / CONFIG_DATA[df.part_name.iloc[i]]["dent_divisor"], 1)
        scaled_total_tear_count = min(df.tear_count.iloc[i] / CONFIG_DATA[df.part_name.iloc[i]]["tear_divisor"], 1)

        scratch_ratio_count = (df.scratch_ratio.iloc[i] + scaled_total_scratch_count)
        dent_ratio_count = (df.dent_ratio.iloc[i] + scaled_total_dent_count)
        tear_ratio_count = (df.tear_ratio.iloc[i] + scaled_total_tear_count)

        total_ratio_count = scratch_ratio_count * acc_scratch + dent_ratio_count * acc_dent + tear_ratio_count * acc_tear

        damage_score = total_ratio_count + score_confidence.iloc[i]

        """
        Scaling damage score to ensure it's from 0 to 100 by dividing by score divisor.
        Score divisor is calculated by taking max of unscaled damage score
        """
        score_divisor = CONFIG_DATA[df.part_name.iloc[i]]["score_divisor"]
        damage_score = (damage_score / score_divisor) * 100
        damage_score_li.append(damage_score)

    df["damage_score"] = damage_score_li

    # Getting rr_cnn_confidence and dnd_cnn_confidence back to original
    df["rr_cnn_confidence"] = 1 - df["rr_cnn_confidence"]
    df["dnd_cnn_confidence"] = 1 - df["dnd_cnn_confidence"]

    return df

def prepare_data(sedna_payload):
    """
    This method fills all relevant values to empty dataframe for passing it to model
    """
    # create place holders for the dataframes
    cnn_data = pd.DataFrame(
        columns=["part_name", "decision", "confidence_repair", "confidence_replace"]
    )
    dnd_data = pd.DataFrame(
        columns=["part_name", "decision", "confidence_damage", "confidence_no_damage"]
    )
    sdt_data = pd.DataFrame(
        columns=[
            "part_name",
            "part_area",
            "scratch_count",
            "dent_count",
            "tear_count",
            "scratch_area",
            "dent_area",
            "tear_area",
        ]
    )
    part_severity_data = pd.DataFrame(
        columns=["part_name", "decision", "confidence"]
    )

    image_level_df = dict()
    image_level_data = pd.DataFrame(
        columns=[
            "car_partial_car",
            "cpc_confidence",
            "position",
            "pos_confidence",
            "hidden_severity",
            "h_sev_confidence",
            "tloss_indicator",
            "tloss_ind_confidence",
            "bumper_misalignment_prediction",
            "bumper_mis_conf"
        ]
    )
    image_id = 0
    
    data = sedna_payload["payload"]
    for image in data:
        # image_id  = image["image_id"]
        # read cnn_model i.e rr cnn model data from payload
        image_id += 1
        cnn_df = pd.DataFrame(image["cnn_model"])
        cnn_df["image_id"] = image_id
        cnn_data = cnn_data.append(cnn_df, ignore_index=True)
        # read dnd model data from payload
        dnd_df = pd.DataFrame(image["dnd_model"])
        dnd_df["image_id"] = image_id
        dnd_data = dnd_data.append(dnd_df, ignore_index=True)
        # read sdt model data from payload
        sdt_df = pd.DataFrame(image["sdt"])
        sdt_df["image_id"] = image_id
        sdt_data = sdt_data.append(sdt_df, ignore_index=True)

        part_severity_df = pd.DataFrame(image["part_severity"])
        part_severity_df["image_id"] = image_id
        part_severity_data = part_severity_data.append(part_severity_df, ignore_index=True)

        # create image_level_df

        image_level_df["car_partial_car"] = image["is_car"]["value"]
        image_level_df["cpc_confidence"] = image["is_car"]["confidence"]
        image_level_df["position"] = image["position"]["value"]
        image_level_df["pos_confidence"] = image["position"]["confidence"]

        image_level_df["hidden_severity"] = image["hidden_severity"]["value"]
        image_level_df["h_sev_confidence"] = image["hidden_severity"]["confidence"]
        image_level_df["tloss_indicator"] = image["is_total_loss"]["value"]
        image_level_df["tloss_ind_confidence"] = image["is_total_loss"]["confidence"]

        # TODO is bumper model sending any info when no bumper is found?
        try:
            image_level_df["bumper_misalignment_prediction"] = image["is_bumper_misaligned"]["value"]
            image_level_df["bumper_mis_conf"] = image["is_bumper_misaligned"]["confidence"]
        except Exception as e:
            image_level_df["bumper_misalignment_prediction"] = 0
            image_level_df["bumper_mis_conf"] = 0
        #######################################################
        image_level_df["image_id"] = image_id
        image_level_data = image_level_data.append(
            pd.DataFrame(image_level_df, index=[image_id]), ignore_index=True
        )
    sdt_data["image_id_part_name"] = (
            sdt_data["image_id"].astype(str) + sdt_data["part_name"]
    )

    part_severity_data["image_id_part_name"] = (
            part_severity_data["image_id"].astype(str) + part_severity_data["part_name"]
    )

    cnn_data["image_id_part_name"] = (
            cnn_data["image_id"].astype(str) + cnn_data["part_name"]
    )
    cnn_data.rename(columns={"confidence_repair": "rr_cnn_confidence"}, inplace=True)
    dnd_data["image_id_part_name"] = (
            dnd_data["image_id"].astype(str) + dnd_data["part_name"]
    )
    dnd_data.rename(
        columns={"confidence_no_damage": "dnd_cnn_confidence"}, inplace=True
    )

    combined_data = image_level_data[
        [
            "image_id",
            "car_partial_car",
            "cpc_confidence",
            "position",
            "pos_confidence",
            "hidden_severity",
            "h_sev_confidence",
            "tloss_indicator",
            "tloss_ind_confidence",
            "bumper_misalignment_prediction",
            "bumper_mis_conf"
        ]
    ]
    combined_data = pd.concat(
        [
            combined_data,
            pd.get_dummies(
                combined_data[
                    ["car_partial_car",
                     "position",
                     "hidden_severity"]
                ]
            ),
        ],
        axis=1,
    )
    combined_data = combined_data.fillna(0)  # filling with 0 where model info is not available
    combined_data = pd.merge(
        combined_data,
        dnd_data[["image_id", "part_name", "dnd_cnn_confidence"]],
        on="image_id",
        how="inner",
    )

    combined_data["image_id_part_name"] = (
            combined_data["image_id"].astype(str) + combined_data["part_name"]
    )

    for image_id in combined_data.image_id_part_name.tolist():
        part_name_b = combined_data[combined_data['image_id_part_name'] == image_id]['part_name'].values[0]
        if (part_name_b != 'bumper') & (part_name_b != 'rear_bumper'):
            combined_data.loc[(combined_data['image_id_part_name'] == image_id), 'bumper_misalignment_prediction'] = 0.0
            combined_data.loc[(combined_data['image_id_part_name'] == image_id), 'bumper_mis_conf'] = 0.0

    combined_data = pd.merge(
        combined_data,
        cnn_data[["image_id_part_name", "rr_cnn_confidence"]],
        on="image_id_part_name",
        how="inner",
    )

    combined_data = pd.merge(
        combined_data,
        sdt_data[
            [
                "image_id_part_name",
                "scratch_count",
                "dent_count",
                "tear_count",
                "part_area",
                "scratch_area",
                "dent_area",
                "tear_area",
            ]
        ],
        on="image_id_part_name",
        how="inner")
    combined_data = combined_data[
        combined_data["part_name"].isin(CONFIG_DATA["allowed_parts_list"]
        )
    ]
    combined_data = pd.merge(
        combined_data,
        part_severity_data[['decision', 'confidence', 'image_id_part_name']],
        on="image_id_part_name",
        how="inner",
    )
    combined_data.rename(columns={"decision": "severity_decision", "confidence": "severity_confidence"}, inplace=True)
    combined_data = calculate_damage_score(combined_data)

    dummified_data = combined_data
    dummified_data.loc[
        dummified_data["car_partial_car"] == "car", "car_partial_car_car"
    ] = dummified_data["cpc_confidence"]
    dummified_data.loc[
        dummified_data["car_partial_car"] == "partial_car",
        "car_partial_car_partial_car",
    ] = dummified_data["cpc_confidence"]
    dummified_data.loc[
        dummified_data["position"] == "front", "position_front"
    ] = dummified_data["pos_confidence"]
    dummified_data.loc[
        dummified_data["position"] == "front_right", "position_front_right"
    ] = dummified_data["pos_confidence"]
    dummified_data.loc[
        dummified_data["position"] == "front_left", "position_front_left"
    ] = dummified_data["pos_confidence"]
    dummified_data.loc[
        dummified_data["position"] == "rear", "position_rear"
    ] = dummified_data["pos_confidence"]
    dummified_data.loc[
        dummified_data["position"] == "rear_left", "position_rear_left"
    ] = dummified_data["pos_confidence"]
    dummified_data.loc[
        dummified_data["position"] == "rear_right", "position_rear_right"
    ] = dummified_data["pos_confidence"]
    dummified_data.loc[
        dummified_data["position"] == "left", "position_left"
    ] = dummified_data["pos_confidence"]
    dummified_data.loc[
        dummified_data["position"] == "right", "position_right"
    ] = dummified_data["pos_confidence"]

    dummified_data.loc[
        dummified_data["hidden_severity"] == "none", "hidden_severity_none"
    ] = dummified_data["h_sev_confidence"]
    dummified_data.loc[
        dummified_data["hidden_severity"] == "low", "hidden_severity_low"
    ] = dummified_data["h_sev_confidence"]
    dummified_data.loc[
        dummified_data["hidden_severity"] == "medium", "hidden_severity_medium"
    ] = dummified_data["h_sev_confidence"]

    dummified_data = dummified_data.drop(
        [
            "image_id",
            "image_id_part_name",
            "car_partial_car",
            "position",
            "hidden_severity",
        ],
        axis=1,
    )
    dummified_data[
        [
            "tloss_ind_confidence",
            "dnd_cnn_confidence",
            "rr_cnn_confidence",
            "car_partial_car_car",
            "car_partial_car_partial_car",
            "position_front",
            "position_front_left",
            "position_front_right",
            "position_left",
            "position_rear",
            "position_rear_left",
            "position_rear_right",
            "position_right",
            "hidden_severity_low",
            "hidden_severity_medium",
            "hidden_severity_none",
            "bumper_mis_conf"
        ]
    ] = dummified_data[
        [
            "tloss_ind_confidence",
            "dnd_cnn_confidence",
            "rr_cnn_confidence",
            "car_partial_car_car",
            "car_partial_car_partial_car",
            "position_front",
            "position_front_left",
            "position_front_right",
            "position_left",
            "position_rear",
            "position_rear_left",
            "position_rear_right",
            "position_right",
            "hidden_severity_low",
            "hidden_severity_medium",
            "hidden_severity_none",
            "bumper_mis_conf"
        ]
    ].apply(
        pd.to_numeric, errors="coerce"
    )
    dummified_data = dummified_data.drop(
        [
            "cpc_confidence",
            "pos_confidence",
            "h_sev_confidence",
            "tloss_indicator",
        ],
        axis=1,
    )

    dummified_data = dummified_data.fillna(0)  # where the pivoting introduced na
    dummified_data["dnd_cnn_confidence_max"] = dummified_data["dnd_cnn_confidence"]
    dummified_data["dnd_cnn_confidence_min"] = dummified_data["dnd_cnn_confidence"]
    dummified_data["dnd_cnn_confidence_median"] = dummified_data["dnd_cnn_confidence"]

    dummified_data["rr_cnn_confidence_median"] = dummified_data["rr_cnn_confidence"]
    dummified_data["rr_cnn_confidence_max"] = dummified_data["rr_cnn_confidence"]
    dummified_data["rr_cnn_confidence_min"] = dummified_data["rr_cnn_confidence"]

    dummified_data["tloss_ind_confidence_median"] = dummified_data["tloss_ind_confidence"]
    dummified_data["tloss_ind_confidence_max"] = dummified_data["tloss_ind_confidence"]
    dummified_data["tloss_ind_confidence_min"] = dummified_data["tloss_ind_confidence"]

    dummified_data['damage_score_min'] = dummified_data['damage_score']
    dummified_data['damage_score_max'] = dummified_data['damage_score']
    dummified_data['damage_score_median'] = dummified_data['damage_score']
    dummified_data['damage_score'] = dummified_data['damage_score']
    part_severity = dummified_data.copy(deep=True)
    # Replacing with numbers help to select the worst decision
    part_severity["severity_decision"] = part_severity["severity_decision"].str.replace("none", "0"). \
        replace("low", "1").replace("med-high", "2")

    part_severity = part_severity.sort_values(["part_name", "severity_decision"], ascending=False)

    part_severity = part_severity.drop_duplicates(subset="part_name", keep="first")
    part_severity["severity_decision"] = part_severity["severity_decision"].str.replace("0", "none"). \
        replace("1", "low").replace("2", "med_high")

    part_severity["severity_none"] = float(0)
    part_severity["severity_low"] = float(0)
    part_severity["severity_med_high"] = float(0)
    part_severity.reset_index(inplace=True, drop=True)
    for i in range(len(part_severity)):
        part_severity.at[i, "severity_" + part_severity.iloc[i]["severity_decision"]] = part_severity.iloc[i][
            "severity_confidence"]

    dummified_data_pivot = pd.pivot_table(
        dummified_data,
        index=["part_name"],
        aggfunc={
            "tloss_ind_confidence": "mean",
            "tloss_ind_confidence_max": "max",
            "tloss_ind_confidence_min": "min",
            "tloss_ind_confidence_median": "median",
            "dnd_cnn_confidence": "mean",
            "rr_cnn_confidence": "mean",
            "dnd_cnn_confidence_max": "max",
            "dnd_cnn_confidence_min": "min",
            "dnd_cnn_confidence_median": "median",
            "rr_cnn_confidence_median": "median",
            "rr_cnn_confidence_max": "max",
            "rr_cnn_confidence_min": "min",
            "car_partial_car_car": "max",
            "car_partial_car_partial_car": "max",
            "position_front": "max",
            "position_front_left": "max",
            "position_front_right": "max",
            "position_left": "max",
            "position_rear": "max",
            "position_rear_left": "max",
            "position_rear_right": "max",
            "position_right": "max",  
            "hidden_severity_low": "max",
            "hidden_severity_medium": "max",
            "hidden_severity_none": "max",
            "scratch_count": "max",
            "dent_count": "max",
            "tear_count": "max",
            "part_area": "max",
            "scratch_area": "max",
            "dent_area": "max",
            "tear_area": "max",
            "bumper_misalignment_prediction": "max",
            "bumper_mis_conf": "max",
            "damage_score_min": "min",
            "damage_score_max": "max",
            "damage_score_median": "median",
            "damage_score": "mean"
        },
    )
    dummified_data_pivot = dummified_data_pivot.reset_index()
    dummified_data_pivot = pd.concat(
        [dummified_data_pivot, pd.get_dummies(dummified_data_pivot["part_name"])],
        axis=1,
    )

    dummified_data_pivot.rename(
        columns={
            "scratch_count": "total_scratch_count",
            "dent_count": "total_dent_count",
            "tear_count": "total_tear_count",
            "scratch_area": "total_scratch_area",
            "dent_area": "total_dent_area",
            "tear_area": "total_tear_area",
        },
        inplace=True,
    )
    dummified_data_pivot["total_sdt_count"] = (
            dummified_data_pivot["total_scratch_count"].astype(int)
            + dummified_data_pivot["total_dent_count"].astype(int)
            + dummified_data_pivot["total_tear_count"].astype(int)
    )
    dummified_data_pivot["total_sdt_area"] = (
            dummified_data_pivot["total_scratch_area"].astype(float)
            + dummified_data_pivot["total_dent_area"].astype(float)
            + dummified_data_pivot["total_tear_area"].astype(float)
    )
    dummified_data_pivot["dent_ratio"] = dummified_data_pivot["total_dent_area"] / dummified_data_pivot["part_area"]
    dummified_data_pivot["scratch_ratio"] = dummified_data_pivot["total_scratch_area"] / dummified_data_pivot[
        "part_area"]
    dummified_data_pivot["tear_ratio"] = dummified_data_pivot["total_tear_area"] / dummified_data_pivot["part_area"]

    for feature in feature_list:
        if feature not in dummified_data_pivot.columns:
            dummified_data_pivot[feature] = 0
    merged_dummified_data_pivot = pd.merge(dummified_data_pivot,
                                           part_severity[
                                               ['part_name', 'severity_none', 'severity_low', 'severity_med_high']],
                                           on=['part_name'])

    return merged_dummified_data_pivot  



def get_sedna_claim_level_data(sedna_payload):
    sedna_results = pd.DataFrame(sedna_payload["result"])
#     print(sedna_results)
    sedna_results.columns = ["part_name","sedna_decisions","sedna_confidence"]
    claim_level_data = prepare_data(sedna_payload)
    claim_level_data = claim_level_data.rename(columns={"severity_none":"part_severity_none","severity_low":"part_severity_low","severity_med_high":"part_severity_med_high"})
    combined_df = pd.merge(claim_level_data, sedna_results, on=["part_name"], how="inner")
    return combined_df.to_json(orient="records")


### IMAGE LEVEL OUTPUT ###

def get_image_level(sedna_input):
    id_li,image_key_li,severity_li,hiddenSeverity_li,position_li,is_car_li,url =[],[],[],[],[],[], []
    for i in range(len(sedna_input["images"])):
        rec=sedna_input["images"][i]
        if not (rec['results']['isCar'] == 'non_car') :
            id_li.append(rec["id"])
            image_key_li.append(rec['image_key'])
            severity_li.append(rec['results']['severity'])
            hiddenSeverity_li.append(rec['results']['hiddenSeverity'])
            position_li.append(rec['results']['position'])
            is_car_li.append(rec['results']['isCar'])
            url.append(rec['url'])
        # print(url)
    df=pd.DataFrame()

    df["id"]=id_li
#     df["image_key"]=image_key_li
    df["image_severity"]=severity_li
    df["hiddenSeverity"]=hiddenSeverity_li
    df["position"]=position_li
    df["is_car"]=is_car_li
    df["url"] = url

    # df = df.replace(to_replace= r'\\', value= '', regex=True)
    result = df.to_dict("records")
    result = json.dumps(result)
    
    
    return result

def getImageQualityIndex(sedna_input):
    im = []
    url = []
    iqi = []
    for i in sedna_input["payload"]:
    #     print(i["image_id"])
    #     print(i["url"])
    #     df["image_id"] = i["image_id"]
    #     df["url"] = i["url"]
        im.append(i["image_id"])
        url.append(i["url"])
        iqi_generator.Model.read_image(i["url"])
        brightness_factor = iqi_generator.Model.brightness(i["url"])
        sharpness_factor = iqi_generator.Model.sharpness_measure(i["url"])
        result = iqi_generator.Model.iqi(i["url"], brightness_factor, sharpness_factor)
        iqi.append(result)
        df = pd.DataFrame(list(zip(im, url, iqi)),
                  columns=['image_id','url', 'IQI'])
        
    return df.to_json(orient="records")


## Pradyumna's codes
def part_data_prep(im,col):
    """
    im: the json data loaded from RAM's code output
    col: feature name that you need to prepare the data for
         {'rrcnn_detail','part_detail','dnd_detail'}
    """
    im = pd.concat([im[['img_id']],pd.DataFrame(im[col].explode())],axis=1).reset_index().drop(['index'],1)
    im = pd.concat([im['img_id'],pd.json_normalize(im[col])],axis=1)
    im['image_id_part_name'] = im['img_id'].astype(str)+'_'+im['part_name']
    im.set_index('image_id_part_name',inplace=True)
    im.drop_duplicates(inplace=True)
    return im

def part_damage_score(im,CONFIG_DATA):
    """
    Input:
    --------
    filename: the path of json file from RAM's code output
    CONFIG_DATA: already read config.json file
    
    Output:
    -------
    json file with image id, partname and damage score 
    
    *known issue : pd.to_json() sometimes uses ' for nested keys instead of ".
    
    """
    # with open(filename, "r") as read_file:
    #     im = json.load(read_file)

    im = pd.json_normalize(im['part-level-output'])
    
    rr = part_data_prep(im,col='rrcnn_detail')
    sdt = part_data_prep(im,col='part_detail')
    dnd = part_data_prep(im,col='dnd_detail')
    
    df = pd.concat([sdt,dnd[['confidence_damage']],rr[['confidence_replace']]],axis=1)   
    
    df["dent_ratio"] = df["total_dent_area"] / df["part_area"]
    df["scratch_ratio"] = df["total_scratch_area"] / df["part_area"]
    df["tear_ratio"] = df["total_tear_area"] / df["part_area"]
    
    df.loc[df['scratch_ratio'] > 1, 'scratch_ratio'] = 1
    df.loc[df['dent_ratio'] > 1, 'dent_ratio'] = 1
    df.loc[df['tear_ratio'] > 1, 'tear_ratio'] = 1
        
    damage_score_li = []
    
    for i in range(len(df)):
        
        acc_dnd = CONFIG_DATA[df.part_name.iloc[i]]["acc_dnd"]
        acc_tear = CONFIG_DATA[df.part_name.iloc[i]]["acc_tear"]
        acc_dent = CONFIG_DATA[df.part_name.iloc[i]]["acc_dent"]
        acc_scratch = CONFIG_DATA[df.part_name.iloc[i]]["acc_scratch"]
        acc_rrcnn = CONFIG_DATA[df.part_name.iloc[i]]["acc_rrcnn"]
        
        """
        Score confidence is calculated as weighted sum of dnd_cnn_confidence and rr_cnn_confidence.
        Accuracy of dnd model and rr_cnn model is approximately considered as weights.
        """
        score_confidence = df["confidence_damage"] * acc_dnd + df["confidence_replace"] * acc_rrcnn
        
        """
        Ensuring scaled SDT counts are between 0 and 1 by diving with SDT divisor.
        Taking min to ensure they do not exceed 1.
        SDT divisor is calculated by taking max of it's count which is occurring in atleast 1% of the data.
        """
        scaled_total_scratch_count = min(df.scratch_count.iloc[i]/CONFIG_DATA[df.part_name.iloc[i]]["scratch_divisor"], 1)
        scaled_total_dent_count = min(df.dent_count.iloc[i]/CONFIG_DATA[df.part_name.iloc[i]]["dent_divisor"], 1)
        scaled_total_tear_count = min(df.tear_count.iloc[i]/CONFIG_DATA[df.part_name.iloc[i]]["tear_divisor"], 1)
        
        scratch_ratio_count = (df.scratch_ratio.iloc[i] + scaled_total_scratch_count)
        dent_ratio_count = (df.dent_ratio.iloc[i] + scaled_total_dent_count)
        tear_ratio_count = (df.tear_ratio.iloc[i] + scaled_total_tear_count)
        
        total_ratio_count = scratch_ratio_count * acc_scratch + dent_ratio_count * acc_dent + tear_ratio_count * acc_tear
        damage_score = total_ratio_count + score_confidence.iloc[i]
        """
        Scaling damage score to ensure it's from 0 to 100 by dividing by score divisor.
        Score divisor is calculated by taking max of unscaled damage score
        """
        score_divisor = CONFIG_DATA[df.part_name.iloc[i]]["score_divisor"]
        
        damage_score = (damage_score / score_divisor) * 100
        damage_score_li.append(damage_score)
    
    df["damage_score"] = damage_score_li

    return df[['img_id','part_name',"damage_score"]].set_index(['img_id','part_name']).to_json()






# get part-level data
def get_part_data(input_data):
    """
    get_part_data

    """

    img_lvl_list = input_data["images"]
    final_list = []

    for img in img_lvl_list:
        data_dict = dict()
        data_dict["img_id"] = img["id"]
        part_list = []
        if not img["results"]["isCar"] == 'non_car':
            for part in  img["results"]["damageCounts"] :
                if part["part_name"] in allowed_part_list:
                        part_dict = dict()
                        part_dict["part_name"] = part["part_name"]
                        part_dict["scratch_count"] = part["scratch_count"]
                        part_dict["dent_count"] = part["dent_count"]
                        part_dict["tear_count"] = part["tear_count"]
                        part_dict["part_area"] = part["part_area"]
                        part_dict["part_severity"] = part["part_severity"]

                        total_scratch_area = 0.0
                        total_dent_area = 0.0
                        total_tear_area = 0.0
                        scratch_polygons = []
                        dent_polygons = []
                        tear_polygons = []

                        if len(part["damages"])!= 0:
                            for damage in part["damages"]:
                                if damage["damage_type"] == "scratch":
                                    total_scratch_area += damage["damage_area"]
                                    scratch_polygons.append(damage["damage_polygon"])
                                if damage["damage_type"] == "dent":
                                    total_dent_area += damage["damage_area"]
                                    dent_polygons.append(damage["damage_polygon"])
                                if damage["damage_type"] == "tear":
                                    total_tear_area += damage["damage_area"]
                                    tear_polygons.append(damage["damage_polygon"])

                        part_dict["total_scratch_area"] = total_scratch_area
                        part_dict["total_dent_area"] = total_dent_area
                        part_dict["total_tear_area"] = total_tear_area

                        # part_dict["scratch_polygons"] = scratch_polygons
                        # part_dict["dent_polygons"] = dent_polygons
                        # part_dict["tear_polygons"] = tear_polygons


        
                        part_list.append(part_dict)
            data_dict["part_detail"] = part_list
            data_dict["dnd_detail"] =  eval(img["results"]["damageNoDamage"])
            data_dict["rrcnn_detail"] = eval(img["results"]["consolidatedPartsReplaceRepair"])
            # data_dict["part_lvl_sev_detail"] = eval(img["results"]["partLevelSeverity"]) 
            all_data = eval(img["results"]["partLevelSeverity"]) 
            part_lvl_sev_list = []
            for data in all_data:
                if data["part_name"] in allowed_part_list:
                    part_lvl_sev_list.append(data)
            data_dict["part_lvl_sev_detail"] = part_lvl_sev_list

            

            final_list.append(data_dict)
        # calculate damage-score from Pradyumna's function
    im = {"part-level-output": final_list}
    damage_scr_detail = part_damage_score(im,CONFIG_DATA)
    final_list.append(eval(damage_scr_detail))
    return final_list


def upload_images(imgs_list: list, upload_api: str, token: str) -> dict:
    """upload_images using the  given upload api

    Args:
        imgs_list (list): list of images to upload
        upload_api (str): the api for uploading the images to s3
        token (str): token for uploading

    Returns:
        keys (dict): dict of submitted imagenames,
                received keys, and created uuid names.
    """
    keys = {"keys": [], "img": [], "uuid": [], "origname": []}

    if not isinstance(imgs_list, list):
        raise ValueError(
            "Expecting list for image_list but received %s", type(img_list)
        )
    headers = {"Authorization": token}
    for img in imgs_list:
            

        try:
            img = eval(img)
            # print(img)
            # files = {"file": open(img, "rb")}
            name = img['name']
            with open('temp.jpg','wb') as f:
                f.write(base64.b64decode(img['base64'].split(",",1)[1]))
            directory = os.getcwd()
            print("current directory", directory)
            img = os.path.join(directory,"temp.jpg")
            files = {"file": open(img, "rb")}
            # print(files)
            # creating random strings for name as s3 compatible names
            uuid_ = "-".join(str(uuid.uuid4()).split("-")[:3]) + ".jpg"
            claim_id = str(uuid.uuid1()).split('-')[0]
            # print(claim_id)
            data = {"filename": claim_id+"_"+uuid_}
            # print("Heders and data",headers,data)
            response_ = requests.post(
                upload_api, files=files, data=data, headers=headers
            )
            # print(response_.json())
            keys["keys"].append(response_.json()["data"]["key"])
            keys["img"].append(img)
            keys["uuid"].append(uuid_)
            keys["origname"].append(name)
        except:
            print("exception")
            continue
    # print(keys)
    header = ["claim_id", "image_url","original_name","pub_s3url","time"]
    filename = str(datetime.date.today())+".csv"
    filepath = os.path.join("../data/uploads",Path(filename))
    write_header=False
    if not os.path.exists(filepath):
        write_header=True
    with open(filepath, 'a', newline ='') as file:
        writer = csv.writer(file, delimiter=',')
        if write_header:
            writer.writerow(i for i in header)
        for j,i in zip(keys["keys"],keys["origname"]):
            writer.writerow([claim_id,j,i,"",datetime.datetime.now()])
    uid = run_pipeline.process_claim()
    print("printing UID",uid)

    # run_pipeline.get_claim(uid)
    return keys, uid


def get_claim(uid):
    status = run_pipeline.get_claim(uid)
    if status in [2,3]:
        run_pipeline.get_sedna(uid)

    return {"status" : status}
