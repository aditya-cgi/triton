from fastapi import FastAPI
from fastapi.exceptions import RequestValidationError
from fastapi.responses import JSONResponse
# import sednaImageInput
import logging
from pydantic import BaseModel
from typing import Dict, Optional
from fastapi.middleware.cors import CORSMiddleware
import helper

app = FastAPI()
logger = logging.getLogger("api")

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


# Claim level sedna payload as input
class SednaPayload(BaseModel):
    success:Optional[bool] =None
    message:Optional[str] =None
    data :Optional[Dict] = None
        
class Payload(BaseModel):
    success:Optional[bool] =None
    message:Optional[str] =None
    data :Optional[Dict] = None

class imageUpload(BaseModel):
    imgs_list:Optional[list] =None
    upload_api:Optional[str] =None
    token :Optional[str] = None

class getclaim(BaseModel):
    uid :Optional[str] = None

    
               
@app.post('/sedna-sedan/getSednaClaimLevelData')
async def getSednaClaimLevelData(payload:SednaPayload):
    # Recieve data from request in JSON format
    logger.debug("Received request")
    input_data = payload.data
    logger.debug("input data -")
    logger.debug(input_data)
    try:
        response = helper.get_sedna_claim_level_data(input_data)
        return response

    except Exception as e:
        logger.error(e)
        error_response = {
            "success": False,
            
            "input_data":input_data
          
        }
        return JSONResponse(status_code=500, content=error_response)
    
@app.post('/sedna-sedan/getImageIQI')
async def getImageIQI(payload:SednaPayload):
    # Recieve data from request in JSON format
    logger.debug("Received request")
    input_data = payload.data
    logger.debug("input data -")
    logger.debug(input_data)
    try:
        response = helper.getImageQualityIndex(input_data)

        return response

    except Exception as e:
        logger.error(e)
        error_response = {
            "success": False,
            
            "input_data":input_data
          
        }
        return JSONResponse(status_code=500, content=error_response)


# Get claim api output as payload
@app.post('/sedna-sedan/getImageInput')
async def getImageInput(payload:Payload):
    # Recieve data from request in JSON format
    logger.debug("Received request")
    input_data =payload.data
    logger.debug("input data -")
    logger.debug(input_data)
    try:
        response = helper.get_image_level(input_data)

        return response

    except Exception as e:
        logger.error(e)
        error_response = {
            "success": False,
            
            "input_data":input_data
          
        }
        return JSONResponse(status_code=500, content=error_response)



# Get part-level details
@app.post("/sedna-sedan/get-part-details")
async def Get_part_detail(payload: Payload):
    # Recieve data from request in JSON format
    logger.debug("Received request")
    input_data = payload.data
    logger.debug("input data -")
    logger.debug(input_data)
    try:
        part_level_data = helper.get_part_data(input_data)
        logger.info("parsed data " + str(part_level_data))
        response = {
            "success": True,
            "data": {"part-level-output": part_level_data},
            "message": "part-level-info-parsing completed."
        }
        return response

    except Exception as e:
        logger.error(e)
        error_response = {
            "success": False,
            "data": {},
            "message": str(e),
        }
        return JSONResponse(status_code=500, content=error_response)


# Get part-level details
@app.post("/sedna-sedan/imageUpload")
async def imageUpload(payload: imageUpload):
    # Recieve data from request in JSON format
    logger.debug("Received request")
    input_data = payload
    # print("input data -")
    # print(input_data)
    try:
        Keys = helper.upload_images(payload.imgs_list, payload.upload_api, payload.token )
        logger.info("parsed data " + str(Keys))
        print(Keys)
        response = {
            "success": True,
            "data": {"Keys": Keys},
            "message": "Image Upload completed."
        }
        return response

    except Exception as e:
        logger.error(e)
        error_response = {
            "success": False,
            "data": {},
            "message": str(e),
        }
        return JSONResponse(status_code=500, content=error_response)

# Get part-level details
@app.post("/sedna-sedan/getclaim")
async def getclaim(payload: getclaim):
    # Recieve data from request in JSON format
    logger.debug("Received request")
    input_data = payload.uid
    print("input data -")
    print(input_data)
    try:
        Keys = helper.get_claim(input_data)
        logger.info("parsed data " + str(Keys))
        response = {
            "success": True,
            "data": Keys,
            "message": "Image Upload completed."
        }
        return response

    except Exception as e:
        logger.error(e)
        error_response = {
            "success": False,
            "data": {},
            "message": str(e),
        }
        return JSONResponse(status_code=500, content=error_response)