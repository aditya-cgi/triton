from genericpath import exists
import utils
import json
import os
import csv
import time
import datetime
from pathlib import Path
import pandas as pd
import requests
import helper


def process_claim():
    file_path = '../config/process_config.json'

    with open(file_path) as f:
        config = json.load(f)
    config['file_to_process'] = os.path.join(config['file_to_process'],str(datetime.date.today())+".csv")
    json_file = utils.prepare_claim_data(config)

    write_header = False
    if not os.path.exists(os.path.join(config["save_claim_uid"],str(datetime.date.today())+".csv")):
        write_header = True
    with open(os.path.join(config["save_claim_uid"],str(datetime.date.today())+".csv"), 'a+', newline='') as file:
        fieldnames = ['claim_number', 'uid','make','model','year','vin','loss_year']
        writer = csv.DictWriter(file, fieldnames=fieldnames)
        if write_header:
            writer.writeheader()
        print(json_file)
        with open(json_file) as f:
            claim_data = json.load(f)
            # print(claim_data)
        response = utils.process_one_claim(config, claim_data)
        print(response)
        writer.writerow({"claim_number" : json_file.split('/')[-1].split('.')[0], "uid" : response['data']['id'], \
            "make" : claim_data['make'], "model" : claim_data['model'],"year" : claim_data['year'],"vin" : claim_data['vin'],"loss_year" : claim_data['loss_year']})
        file.flush()
    return(response['data']['id'])


    # time.sleep(3)
def get_claim(uid):   
    file_path = '../config/process_config.json'

    with open(file_path) as f:
        config = json.load(f)
    response_orig = utils.get_claim(config, uid)
    print(response_orig)
    json_object = json.dumps(response_orig, indent = 4) 
    with open(os.path.join("../data/latest_get_claim.json"), "w") as outfile: 
        outfile.write(json_object)
    filename = str(datetime.date.today())+".csv"
    filepath = os.path.join("../data/uploads",Path(filename))
    df = pd.read_csv(filepath)
    for i in response_orig["data"]["images"]:
        print(i["image_key"])
        df.loc[df["image_url"] == i["image_key"], "pub_s3url"] = i["url"]
        df.to_csv(filepath,index=False)
    print(response_orig['data']['status'])
    if response_orig['data']['status'] in [2,3]:
    
        print(type(response_orig), response_orig.keys())

        response = {"imageleveldata" : eval(helper.get_image_level(response_orig['data']))}
        print("--------------------------",response)
        with open("../frontend/data.json", "w") as f:
            json.dump(response, f)

        response = {"partleveldart" : [
        {
                "success": True,
                "data": {"part-level-output": helper.get_part_data(response_orig["data"])},
                "message": "part-level-info-parsing completed."
            }]}
        print("888888888888888888888888888",response)
        # with open(os.path.join("../Team Triton_formated/part-level-output.json"), "w") as outfile: 
        with open("../frontend/part-level-output.json", "w") as f:
            json.dump(response, f)
    return response_orig['data']['status']

    

def get_sedna(uid):   
    file_path = '../config/process_config.json'

    with open(file_path) as f:
        config = json.load(f)
    response = utils.get_sedna_input(config, uid)
    json_object = json.dumps(response, indent = 4) 
    with open(os.path.join("../data/latest_get_sedna.json"), "w") as outfile: 
        outfile.write(json_object)

    response =  {"claimleveldata" : eval(helper.get_sedna_claim_level_data(response["data"]))}
    # json_object = eval(json.dumps(response, indent = 4)) 
    with open("../frontend/sedna_claim_level.json", "w") as f:
        json.dump(response, f)
    
# print(response)

# ### Sedna Input
# claim_number='CD7EYJLC'
# response = utils.get_sedna_input(config, 'CHF1DA2F')

# # ## Save the results in json
# json_object = json.dumps(response, indent = 4) 
#         # Writing to sample.json 
# with open(os.path.join("/home/cgiuser/Varsha/Sedna/ds-sandbox-repo1/unit_cluster/data/sedna_payload/"+claim_number+".json"), "w") as outfile: 
#     outfile.write(json_object) 
    

# Get Claim API
# response = utils.get_claim(config, '9NSC3LU')
# ## Save the results in json
# json_object = json.dumps(response, indent = 4) 
#         # Writing to sample.json 
# with open(os.path.join("/home/cgiuser/Varsha/Sedna/ds-sandbox-repo1/unit_cluster/data/claim_id_12/C9NSC3LU1.json"), "w") as outfile: 
#     outfile.write(json_object) 



# #CZMXPPBQ
# #C5IDC6Y5